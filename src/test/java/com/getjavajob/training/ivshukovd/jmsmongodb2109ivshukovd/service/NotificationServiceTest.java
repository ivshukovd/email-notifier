package com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.service;

import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.dao.NotificationDao;
import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.domain.NotificationByEmail;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

class NotificationServiceTest {

    private NotificationDao notificationDaoMock;
    private NotificationService notificationService;
    private List<NotificationByEmail> notifications;

    @BeforeEach
    void init() {
        notificationDaoMock = mock(NotificationDao.class);
        notificationService = new NotificationService(notificationDaoMock);
        notifications = new ArrayList<>();
    }

    @Test
    void testSaveNotification() {
        NotificationByEmail notification = new NotificationByEmail();
        notification.setEmailForNotification("mail@mail.ru");
        notification.setText("notification next");
        doAnswer(invocation -> {
            notifications.add(notification);
            return null;
        }).when(notificationDaoMock).save(notification);
        notificationService.saveNotification(notification);
        assertEquals(1, notifications.size());
    }

}