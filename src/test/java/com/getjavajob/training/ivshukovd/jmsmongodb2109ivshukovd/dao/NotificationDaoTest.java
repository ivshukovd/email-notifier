package com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.dao;

import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.domain.NotificationByEmail;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataMongoTest
class NotificationDaoTest {

    @Autowired
    private NotificationDao notificationDao;

    @Test
    void testSave() {
        NotificationByEmail notification = new NotificationByEmail();
        notification.setEmailForNotification("mail@mail.ru");
        notification.setText("notification next");
        assertEquals(notification, notificationDao.save(notification));
        assertEquals(1, notificationDao.findAll().size());
        assertEquals("notification next", notificationDao.findAll().get(0).getText());
    }

}