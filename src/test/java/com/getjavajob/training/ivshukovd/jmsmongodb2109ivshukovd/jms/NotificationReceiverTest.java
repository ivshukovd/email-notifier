package com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.jms;

import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.domain.NotificationByEmail;
import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.service.NotificationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class NotificationReceiverTest {

    List<NotificationByEmail> notifications;
    List<MimeMessage> sentMessages;
    private NotificationService notificationServiceMock;
    private JavaMailSender javaMailSenderMock;
    private NotificationReceiver notificationReceiver;

    @BeforeEach
    void init() {
        notificationServiceMock = mock(NotificationService.class);
        javaMailSenderMock = mock(JavaMailSender.class);
        notificationReceiver = new NotificationReceiver(notificationServiceMock, javaMailSenderMock);
        notifications = new ArrayList<>();
        sentMessages = new ArrayList<>();
    }

    @Test
    void testReceiveNotification_NotificationIsNull() {
        NotificationByEmail notification = new NotificationByEmail();
        notification.setEmailForNotification("mail@mail.ru");
        notification.setText("notification next");
        notificationReceiver.receiveNotification(null);
        assertEquals(0, notifications.size());
        assertEquals(0, sentMessages.size());
    }

    @Test
    void testReceiveNotification_socialNetworkUrlExists() throws MessagingException, IOException {
        MimeMessage message = new MimeMessage((Session) null);
        NotificationByEmail notification = new NotificationByEmail();
        notification.setEmailForNotification("mail@mail.ru");
        notification.setText("notification next");
        notification.setSocialNetworkUrl("http://mysocialnetwork.ru");
        doAnswer(invocation -> {
            notifications.add(notification);
            return null;
        }).when(notificationServiceMock).saveNotification(notification);
        when(javaMailSenderMock.createMimeMessage()).thenReturn(message);
        doAnswer(invocation -> {
            sentMessages.add(message);
            return null;
        }).when(javaMailSenderMock).send(message);
        notificationReceiver.receiveNotification(notification);
        System.out.println("Notification: " + notifications.get(0));
        System.out.println("Message: " + sentMessages.get(0).getContent());
        assertEquals(1, notifications.size());
        assertEquals(1, sentMessages.size());
        String expectedContent = "<h3>Dear mail,</h3><p>notification next</p><p>Go to <a href=http://mysocialnetwork"
                + ".ru>IvshukovdSN</a> and find out " + "news</p><br><p>With Best regards, IvshukovdSN</p>";
        assertEquals(expectedContent, sentMessages.get(0).getContent());
    }

    @Test
    void testReceiveNotification_socialNetworkUrlNotExists() throws MessagingException, IOException {
        MimeMessage message = new MimeMessage((Session) null);
        NotificationByEmail notification = new NotificationByEmail();
        notification.setEmailForNotification("mail@mail.ru");
        notification.setText("notification next");
        doAnswer(invocation -> {
            notifications.add(notification);
            return null;
        }).when(notificationServiceMock).saveNotification(notification);
        when(javaMailSenderMock.createMimeMessage()).thenReturn(message);
        doAnswer(invocation -> {
            sentMessages.add(message);
            return null;
        }).when(javaMailSenderMock).send(message);
        notificationReceiver.receiveNotification(notification);
        assertEquals(1, notifications.size());
        assertEquals(1, sentMessages.size());
        String expectedContent = "<h3>Dear mail,</h3><p>notification next</p><br><p>With Best regards, IvshukovdSN</p>";
        assertEquals(expectedContent, sentMessages.get(0).getContent());
    }

}