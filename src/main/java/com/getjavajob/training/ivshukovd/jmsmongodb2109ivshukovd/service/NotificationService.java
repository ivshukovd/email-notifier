package com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.service;

import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.dao.NotificationDao;
import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.domain.NotificationByEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    private final NotificationDao notificationDao;

    @Autowired
    public NotificationService(NotificationDao notificationDao) {
        this.notificationDao = notificationDao;
    }

    public NotificationByEmail saveNotification(NotificationByEmail emailMessage) {
        notificationDao.save(emailMessage);
        return emailMessage;
    }

}