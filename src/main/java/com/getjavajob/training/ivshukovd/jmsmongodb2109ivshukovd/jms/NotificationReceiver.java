package com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.jms;

import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.domain.NotificationByEmail;
import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.service.NotificationService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static org.slf4j.LoggerFactory.getLogger;

@Component
public class NotificationReceiver {

    private final Logger logger = getLogger(NotificationReceiver.class);
    private final NotificationService notificationService;
    private final JavaMailSender mailSender;

    @Autowired
    public NotificationReceiver(NotificationService notificationService, JavaMailSender mailSender) {
        this.notificationService = notificationService;
        this.mailSender = mailSender;
    }

    @JmsListener(destination = "notificationAboutWallPost")
    public void receiveNotification(NotificationByEmail notification) {
        if (nonNull(notification)) {
            logger.info("Notification received: {}", notification);
            notificationService.saveNotification(notification);
            sendEmail(notification);
        }
    }

    private void sendEmail(NotificationByEmail notification) {
        var message = mailSender.createMimeMessage();
        var mimeMessageHelper = new MimeMessageHelper(message, "UTF-8");
        try {
            mimeMessageHelper.setTo(notification.getEmailForNotification());
            mimeMessageHelper.setSubject("Notification from IvshukovdSN");
            var emailBody = new StringBuilder();
            emailBody.append(format("<h3>Dear %s,</h3>", notification.getEmailForNotification().split("@")[0]));
            emailBody.append(format("<p>%s</p>", notification.getText()));
            if (nonNull(notification.getSocialNetworkUrl())) {
                emailBody.append(format("<p>Go to <a href=%s>IvshukovdSN</a> and find out news</p>",
                                        notification.getSocialNetworkUrl()));
            }
            emailBody.append("<br>");
            emailBody.append("<p>With Best regards, IvshukovdSN</p>");
            mimeMessageHelper.setText(emailBody.toString(), true);
            mailSender.send(message);
            logger.info("Email sent successfully");
        } catch (MessagingException e) {
            logger.warn("Email can't be send");
        }
    }

}