package com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.dao;

import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.domain.NotificationByEmail;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NotificationDao extends MongoRepository<NotificationByEmail, String> {

}