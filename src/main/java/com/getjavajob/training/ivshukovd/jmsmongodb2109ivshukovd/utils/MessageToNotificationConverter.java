package com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.domain.NotificationByEmail;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import static org.slf4j.LoggerFactory.getLogger;

public class MessageToNotificationConverter implements MessageConverter {

    private final ObjectMapper mapper;
    Logger logger = getLogger(MessageToNotificationConverter.class);

    public MessageToNotificationConverter() {
        this.mapper = new ObjectMapper();
    }

    @Override
    public Message toMessage(Object object, Session session) {
        var notification = (NotificationByEmail) object;
        try {
            String payload = mapper.writeValueAsString(notification);
            TextMessage message = session.createTextMessage();
            message.setText(payload);
            logger.info("NotificationByEmail converted to JMS Message successfully");
            return message;
        } catch (JsonProcessingException | JMSException e) {
            logger.error("Error converting from NotificationByEmail");
            return new ActiveMQTextMessage();
        }
    }

    @Override
    public Object fromMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        NotificationByEmail notification = null;
        try {
            notification = mapper.readValue(textMessage.getText(), NotificationByEmail.class);
            logger.info("JMS Message converted to NotificationByEmail successfully");
        } catch (JsonProcessingException | JMSException e) {
            logger.error("Error converting to NotificationByEmail");
        }
        return notification;
    }

}