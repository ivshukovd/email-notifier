package com.getjavajob.training.ivshukovd.jmsmongodb2109ivshukovd.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document
public class NotificationByEmail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String emailForNotification;
    private String text;
    private String socialNetworkUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmailForNotification() {
        return emailForNotification;
    }

    public void setEmailForNotification(String emailForNotification) {
        this.emailForNotification = emailForNotification;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSocialNetworkUrl() {
        return socialNetworkUrl;
    }

    public void setSocialNetworkUrl(String socialNetworkUrl) {
        this.socialNetworkUrl = socialNetworkUrl;
    }

    @Override
    public String toString() {
        return String.format("Email for notification: %s, notification text: %s", emailForNotification, text);
    }

}